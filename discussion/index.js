let person = {
	firstName: "John",
	lastName: "Smith",
	location: {
		city: "Tokyo",
		country: "Japan"
	},
	emails: ["john@mail.com", "johnsmith@mail.xyz"]
}

/* Accessing Object Properties

Use the dot notation to access an object's property's value

*/

/*console.log(person.lastName)
console.log(person.emails[0])
console.log(person.location.country)*/

let example = {
	prop1: [
	{
   		prop2: ["cat", "dog"]
	}
	]
}

console.log(example.prop1[0].prop2[1])